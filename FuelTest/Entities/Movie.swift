//
//  Movie.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

enum WatchedState: Int {
    case undefine = -1
    case want
    case watched
    case dontLike
}

extension WatchedState {
    static func toString() -> [String] {
        return ["Want", "Watched", "Don't like this"]
    }
    
    func next() -> WatchedState {
        return WatchedState(rawValue: self.rawValue + 1) ?? self
    }
    
    func previous() -> WatchedState {
        return WatchedState(rawValue: self.rawValue - 1) ?? self
    }
}

protocol Movie {
    var image: UIImage? { get }
    var state: WatchedState { get set }
    var desc: String { get }
}

class MovieModel: Movie {
    let desc: String
    let image: UIImage?
    var state: WatchedState
    
    init(image: UIImage?, state: WatchedState, desc: String) {
        self.desc   = desc
        self.image  = image
        self.state  = state
    }
}


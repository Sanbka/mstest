//
//  StateCell.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

class StateCell: UITableViewCell {
    @IBOutlet weak var statusLabel: UILabel!
}

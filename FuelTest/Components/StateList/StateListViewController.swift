//
//  StateListViewController.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

final class StateListViewController: UIViewController {
    struct ViewConfig {
        let heightStateElement: CGFloat
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    private var selectedItem: Int?
    private var items: [String] = []
    private var viewConfig: ViewConfig = .defaultConfig
    
    init(config: ViewConfig, items:[String]) {
        self.viewConfig = config
        self.items = items
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func update() {
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(nib: UINib(nibName: "StateCell", bundle: nil), withCellClass: StateCell.self)
        self.update()
    }
    
    public func selectItem(index: Int) {
        self.selectedItem = index
        self.update()
    }
}

extension StateListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewConfig.heightStateElement
    }
}

extension StateListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: StateCell.self)
        
        cell.statusLabel.text = self.items[indexPath.row]
        let isSelect = self.selectedItem == indexPath.row
        cell.statusLabel.alpha = isSelect ? 0.6 : 0.1
        
        return cell
    }
}

extension StateListViewController.ViewConfig {
    static var defaultConfig: StateListViewController.ViewConfig {
        return StateListViewController.ViewConfig(heightStateElement: 32)
    }
}

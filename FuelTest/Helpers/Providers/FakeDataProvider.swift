//
//  FakeDataProvider.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

class FakeDataProvider {
    
    static func getMovies() -> [Movie] {
        return [MovieModel(image: UIImage(named: "Poster001")!, state: .undefine, desc: "Based on your rate for ‘Interstellar’"),
                MovieModel(image: UIImage(named: "Poster002")!, state: .undefine, desc: "Based on novel"),
                MovieModel(image: UIImage(named: "Poster003")!, state: .undefine, desc: "Sequel for movie you’ve loved"),
                MovieModel(image: nil, state: .undefine, desc: "")
        ]
    }
}

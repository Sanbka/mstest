//
//  PagingFlowLayout.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 03.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

public var offsetFocusedElement: CGFloat = 30

class PagingFlowLayout: UICollectionViewFlowLayout {
    
    var pageWidth: CGFloat {
        switch scrollDirection {
        case .horizontal:
            return itemSize.width + minimumLineSpacing
        case .vertical:
            return itemSize.height + minimumLineSpacing
        }
    }
    
    var flickVelocity: CGFloat = 0.3
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = self.collectionView else { return CGPoint.zero }
        var contentOffset = proposedContentOffset
        let rawPage = collectionView.contentOffset.x / pageWidth
        let curPage = velocity.x > 0.0 ? floor(rawPage) : ceil(rawPage)
        let nextPage = velocity.x > 0.0 ? ceil(rawPage) : floor(rawPage)
        
        let isNext = abs(1 + curPage - rawPage) > 0.5
        let flicked = abs(velocity.x) > self.flickVelocity
        
        let maxItems = self.collectionView?.numberOfItems() ?? 1
        
        var showItem = 0
        
        if isNext && flicked {
            showItem = Int(nextPage)
        } else {
            showItem = Int(round(rawPage))
        }
        
        showItem = showItem == (maxItems - 1) ?  showItem - 1 : showItem
        contentOffset.x = self.getOffset(index: showItem, pageWidth: self.pageWidth)
        
        return contentOffset
    }
    
    private func getOffset(index: Int, pageWidth: CGFloat) -> CGFloat {
        return CGFloat(index) * pageWidth - offsetFocusedElement
    }
}

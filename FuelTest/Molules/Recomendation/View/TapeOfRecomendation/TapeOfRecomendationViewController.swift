//
//  TapeOfRecomendationViewController.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 30.06.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

let recomendationCellWidth: CGFloat = 260
let recomendationCellHeight: CGFloat = 436

protocol MovableGestureObserver: class {
    func move(offset: CGPoint)
}

enum GestureDirection {
    case up
    case down
    case none
}

class TapeOfRecomendationViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var movableItemObserver: MovableGestureObserver?
    
    var startLocationGesture: CGPoint?
    
    var currentElementState: WatchedState? {
        set {
            guard let index = self.currentFocusedElement,
                let val = newValue else { return }
            self.dataSource.data[index].state = val
        }
        
        get {
            guard let index = self.currentFocusedElement else { return nil }
            return self.dataSource.data[index].state
        }
    }
    
    private var collectionViewFlowLayout: PagingFlowLayout!
    private let dataSource = RecomendationDataSource()
    
    /// Calculates the current focused element.
    private var currentFocusedElement: Int? {
        return self.currentFocusedIndexPath?.row
    }
    
    private var currentFocusedCell: UICollectionViewCell? {
        guard let indexPath = self.currentFocusedIndexPath
        else { return nil }
        
        return collectionView.cellForItem(at: indexPath)
    }
    
    private var currentFocusedIndexPath: IndexPath? {
        guard let collectionView = collectionView else { return nil }
        let currentFocusPoint = CGPoint(x: collectionView.contentOffset.x + offsetFocusedElement + self.collectionViewFlowLayout.pageWidth/2, y: collectionView.contentOffset.y + offsetFocusedElement + self.collectionViewFlowLayout.pageWidth/2)
        
        return collectionView.indexPathForItem(at: currentFocusPoint)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionSetup()
        self.dataSource.update()
    }
}

extension TapeOfRecomendationViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        guard let pan = gestureRecognizer as? UIPanGestureRecognizer else { return false }
        let veloity = pan.velocity(in: pan.view)
        
        return abs(veloity.x) < abs(veloity.y)
    }
}

extension TapeOfRecomendationViewController {
    
    @objc func handlePan(gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: self.collectionView)
        guard let indexPath = self.collectionView.indexPathForItem(at: location),
            let cell = collectionView.cellForItem(at: indexPath),
            let focussedIndexPath = self.currentFocusedIndexPath,
            focussedIndexPath == indexPath else { return }
        
        switch gesture.state {
        case .began:
            self.startLocationGesture = location
        case .changed:
            let offset = location.y - self.startLocationGesture!.y
            guard offset > 0,
            offset < self.collectionView.bounds.height * 0.4 else { return }
            self.offset(CGPoint(x: 0, y: offset), cell: cell)
        default:
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: [], animations: {
                self.offset(CGPoint.zero, cell: cell)
            }) { (completed) in
                self.startLocationGesture = nil
            }
        }
    }
 
    private func offset(_ offset: CGPoint, cell: UICollectionViewCell) {
        if let observer = cell as? MovableGestureObserver {
            observer.move(offset: offset)
        }
        self.movableItemObserver?.move(offset: offset)
        cell.transform = CGAffineTransform(translationX: 0, y: offset.y)
    }
}

extension TapeOfRecomendationViewController {
    private func collectionSetup() {
        self.dataSource.register(collection: self.collectionView)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(TapeOfRecomendationViewController.handlePan(gesture:)))
        panGesture.delegate = self
        self.collectionView.addGestureRecognizer(panGesture)
        self.collectionViewFlowLayout = self.collectionView.collectionViewLayout as? PagingFlowLayout
        self.collectionViewFlowLayout.scrollDirection = .horizontal
        self.collectionViewFlowLayout.minimumLineSpacing = 20
        
        let inset = offsetFocusedElement
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        self.collectionView.contentOffset = CGPoint(x: -inset, y: 0)
        
        self.collectionViewFlowLayout.itemSize = CGSize(
            width: recomendationCellWidth,
            height: recomendationCellHeight
        )
        
        self.dataSource.update()
    }
}

extension TapeOfRecomendationViewController {
    private func move(offset: CGPoint) {
        self.movableItemObserver?.move(offset: offset)
    }
}


//
//  RecomendationDataSource.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 30.06.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit
import SwifterSwift

class RecomendationDataSource: NSObject {
    var data: [Movie] = []
    weak var collection: UICollectionView!
    
    func register(collection: UICollectionView) {
        self.collection = collection
        collection.dataSource = self
        let nib = UINib(nibName: RecomendationCellCollectionViewCell.reuseIdentifier, bundle: nil)
        collection.register(nib, forCellWithReuseIdentifier: RecomendationCellCollectionViewCell.reuseIdentifier)
    }
    
    func update() {
        self.data = FakeDataProvider.getMovies()
        self.collection.reloadData()
    }
}

extension RecomendationDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: RecomendationCellCollectionViewCell.self, for: indexPath)
        
        let item = data[indexPath.row]
        
        cell.posterImageView.image = item.image
        cell.movieDescriptionLabel.text = item.desc
        
        return cell
    }
}

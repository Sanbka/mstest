//
//  RecomendationCellCollectionViewCell.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 30.06.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

private let movieDescriptionLabelAlpha: CGFloat = 0.3

class RecomendationCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    
}

extension RecomendationCellCollectionViewCell: MovableGestureObserver {
    func move(offset: CGPoint) {
        self.movieDescriptionLabel.alpha = allowableOffset > offset.y ? movieDescriptionLabelAlpha : 0
    }
}

//
//  RecomendationViewController.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit
import Foundation

public let widthStateList: CGFloat = 160
public let allowableOffset: CGFloat = 70
public let allowableMinOffsetStateList: CGFloat = 55

let changeValuePerPoints: Int = 16

class RecomendationViewController: UIViewController, RecomendationView {
    @IBOutlet weak var recomendationTitleView: UIView!
    @IBOutlet weak var tapeOfRecomendationConainerView: UIView!
    
    var presenter: RecomendationViewPresenter!
    let stateList = StateListViewController(config: .defaultConfig, items: WatchedState.toString())
    let tapeOfRecomendation = TapeOfRecomendationViewController()
    
    var isEditingState: Bool = false
    var isPositiveOffset: Bool = false

    var lastOffset: CGFloat = 0
    var lastDifferentPoint: CGFloat = 0
    
    var heightStateList: CGFloat {
        return CGFloat(WatchedState.toString().count) * StateListViewController.ViewConfig.defaultConfig.heightStateElement
    }
    var feedbackGenerator : UISelectionFeedbackGenerator? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupContainer()
    }
    
    func selectItem(index: Int) {
        self.stateList.selectItem(index: index)
    }
}

extension RecomendationViewController: MovableGestureObserver {
    
    private func estimateState(_ offset: CGPoint) {
        self.isEditingState = offset.y > allowableOffset || self.isEditingState
        let offsetY = offset.y.rounded(.up)
        guard self.lastOffset != offsetY else {
            self.lastOffset = offsetY
            return
        }
        
        if self.isEditingState {
            let lastIsPos = self.isPositiveOffset
            self.isPositiveOffset = self.lastOffset < offsetY
            
            if lastIsPos != self.isPositiveOffset {
                self.lastDifferentPoint = offsetY
            }
            
            self.changeState(offsetY)
        }
        
        self.lastOffset = offsetY
    }
    
    private func changeState(_ offsetY: CGFloat) {
        if (Int(offsetY) >=  Int(self.lastDifferentPoint) + changeValuePerPoints && self.isPositiveOffset) ||
            (Int(offsetY) <=  Int(self.lastDifferentPoint) - changeValuePerPoints && !self.isPositiveOffset)   {
            var state: WatchedState = .undefine
            if self.isPositiveOffset {
                state = self.presenter.state.next()
            } else {
                state = self.presenter.state.previous()
            }
            
            guard state != self.presenter.state else { return }
            
            self.presenter.state = state
            self.tapeOfRecomendation.currentElementState = state
            self.lastDifferentPoint = offsetY
            
            self.tapticSelectionChanged()
        }
    }
    
    private func tapticSelectionChanged() {
        // TODO: Test the Taptic Engine
        self.feedbackGenerator?.selectionChanged()
        self.feedbackGenerator?.prepare()
    }
    
    func move(offset: CGPoint) {
        let offsetY = offset.y > allowableOffset ? allowableOffset : offset.y
        let k = allowableOffset
        let alpha: CGFloat = 1 - offsetY / k
        
        self.recomendationTitleView.transform = CGAffineTransform(translationX: 0, y: offsetY)
        self.recomendationTitleView.alpha = alpha
        
        if self.feedbackGenerator == nil {
            self.feedbackGenerator = UISelectionFeedbackGenerator()
            self.feedbackGenerator?.prepare()
        }
        
        if offsetY > allowableMinOffsetStateList {
            self.setupStateList(offsetY, k)
        } else {
            self.stateList.view.removeFromSuperview()
            self.isEditingState = false
            self.feedbackGenerator = nil
        }
        
        self.estimateState(offset)
    }
}

extension RecomendationViewController {
    private func setupContainer() {
        self.tapeOfRecomendationConainerView.addSubview(self.tapeOfRecomendation.view)
        self.tapeOfRecomendation.view.frame = self.tapeOfRecomendationConainerView.bounds
        self.addChild(self.tapeOfRecomendation)
        self.tapeOfRecomendation.didMove(toParent: self)
        self.tapeOfRecomendation.view.fillToSuperview()
        self.tapeOfRecomendation.movableItemObserver = self
    }
    
    private func setupStateList(_ offsetY: CGFloat, _ k: CGFloat) {
        if self.stateList.view.superview == nil {
            self.view.addSubview(self.stateList.view)
            self.presenter.state = self.tapeOfRecomendation.currentElementState ?? .undefine
            self.stateList.view.frame = CGRect(x: self.recomendationTitleView.frame.origin.x,
                                               y: self.recomendationTitleView.frame.origin.y - self.heightStateList + offsetY,
                                               width: widthStateList,
                                               height: self.heightStateList)
        }
        self.stateList.view.transform = CGAffineTransform(translationX: 0, y: offsetY - allowableMinOffsetStateList)
        self.stateList.view.alpha = (offsetY - allowableMinOffsetStateList) / (k - allowableMinOffsetStateList)
        self.presenter.state = self.tapeOfRecomendation.currentElementState ?? .undefine
    }
}

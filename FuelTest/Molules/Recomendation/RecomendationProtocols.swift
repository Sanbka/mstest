//
//  RecomendationProtocols.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 06/10/2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

protocol RecomendationView: MovableGestureObserver  {
    func selectItem(index: Int)
}

protocol RecomendationViewPresenter: class {
    var state: WatchedState {get set}
   
    func viewIsReady()
}

protocol RecomendationModuleRouter: class {
    func embedTapeRecomendationModule() -> UIViewController
}

protocol RecomendationService: class {
    
}

protocol RecomendationViewPresenterState {
    var watchedState: WatchedState { get set }
}


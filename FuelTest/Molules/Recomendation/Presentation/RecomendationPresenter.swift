//
//  RecomendationPresenter.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 06/10/2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import Foundation

class RecomendationPresenter: RecomendationViewPresenter {
    var state: WatchedState = .undefine {
        didSet {
            self.view.selectItem(index: state.rawValue)
        }
    }
    
    unowned let view: RecomendationView
    let router: RecomendationModuleRouter
    
    init(view: RecomendationView, router: RecomendationModuleRouter) {
        self.view = view;
        self.router = router
    }
    
    func viewIsReady() {
        
    }
}

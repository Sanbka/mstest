//
//  RecomendationRouter.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 06/10/2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

class RecomendationRouter: RecomendationModuleRouter {
    func configure() -> UIViewController {
        let view = RecomendationViewController()
        let presenter = RecomendationPresenter(view: view, router: self)
        view.presenter = presenter
        
        return view
    }
    
    func embedTapeRecomendationModule() -> UIViewController {
        return TapeOfRecomendationViewController()
    }
}

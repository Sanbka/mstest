//
//  ExploreViewController.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 03.07.2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController, ExploreView {
    
    var presenter: ExploreViewPresenter!
    
    @IBOutlet weak var recomendationContainerView: UIView!
    
    private func addContentView(content: UIViewController) {
        self.recomendationContainerView.addSubview(content.view)
        self.addChild(content)
        content.didMove(toParent: self)
        content.view.fillToSuperview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter.viewIsReady()
    }
    
    func setupContent(content: UIViewController) {
        self.addContentView(content: content)
    }
}

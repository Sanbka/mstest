//
//  ExploreProtocols.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01/10/2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

protocol ExploreView: class {
    func setupContent(content: UIViewController)
}

protocol ExploreViewPresenter {
    func viewIsReady()
}

protocol ExploreModuleRouter {
    func embedRecomendationModule() -> UIViewController
}

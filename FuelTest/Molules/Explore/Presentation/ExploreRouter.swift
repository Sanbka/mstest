//
//  ExploreRouter.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01/10/2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import UIKit

class ExploreRouter: ExploreModuleRouter {
    func configure() -> UIViewController  {
        let view = ExploreViewController()
        let presenter = ExplorePresenter(view: view, router: self)
        view.presenter = presenter
        
        return view
    }
    
    func embedRecomendationModule() -> UIViewController {
        let router = RecomendationRouter()
        return router.configure()
    }
}

//
//  ExplorePresenter.swift
//  FuelTest
//
//  Created by Alexander Drovnyashin on 01/10/2018.
//  Copyright © 2018 Alexander Drovnyashin. All rights reserved.
//

import Foundation

class ExplorePresenter: ExploreViewPresenter {
    unowned let view: ExploreView
    let router: ExploreModuleRouter
    
    init(view: ExploreView, router: ExploreModuleRouter) {
        self.view = view
        self.router = router
    }
    
    func viewIsReady() {
        self.embedRecomendationModule()
    }
    
    func embedRecomendationModule() {
        self.view.setupContent(content: self.router.embedRecomendationModule())
    }
}


